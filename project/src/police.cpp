#include "orx.h"

orxOBJECT *ship;
orxOBJECT *shipBulletSpawnerOwner;
orxOBJECT *alienSpawnerOwner;

/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;

  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  return eResult;
}

void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
  if (ship) {
    orxConfig_PushSection("Game");
    orxFLOAT rotation = orxConfig_GetFloat("ShipVelocity");
    orxConfig_PopSection();

    if (orxInput_IsActive("GoAnticlockwise") ) {
      orxObject_SetAngularVelocity(ship, -rotation);
    }

    if (orxInput_IsActive("GoClockwise") ) {
      orxObject_SetAngularVelocity(ship, rotation);
    }

    if (orxInput_HasNewStatus("Fire")) {
      orxObject_Enable(shipBulletSpawnerOwner, orxInput_IsActive("Fire"));
    }
  }
}

void CreateExplosionAtObject(orxOBJECT *object)
{
  if (object == orxNULL)
    return;

  orxVECTOR objectVector;
  orxObject_GetPosition(object, &objectVector);
  objectVector.fZ = -0.1;

  orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");

  orxObject_SetPosition(explosion, &objectVector);

  /* Rotation copied so that it starts at exactly the same place as the alien */
  orxFLOAT rotation = orxObject_GetRotation(object);
  orxObject_SetRotation(explosion, rotation);
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
  if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

    if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
      orxOBJECT *pstRecipientObject, *pstSenderObject;

      /* Gets colliding objects */
      pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
      pstSenderObject = orxOBJECT(_pstEvent->hSender);

      if (orxString_Compare(orxObject_GetName(pstSenderObject), "ShipBulletObject") == 0) {
        CreateExplosionAtObject(pstRecipientObject);
        orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }
      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "ShipBulletObject") == 0) {
        CreateExplosionAtObject(pstSenderObject);
        orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }

      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "WallObject") == 0
           ||  orxString_Compare(orxObject_GetName(pstSenderObject), "WallObject") == 0) {
        orxEvent_RemoveHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
        CreateExplosionAtObject(ship);
        orxObject_SetLifeTime(ship, 0);
        ship = orxNULL;
        orxObject_SetLifeTime(alienSpawnerOwner, 0);
        alienSpawnerOwner = orxNULL;
      }
    }
  }

  return orxSTATUS_SUCCESS;
}

void orxFASTCALL Exit()
{
  orxLOG("=================== EXIT ================");
}


/** Inits the tutorial
 */
orxSTATUS orxFASTCALL Init()
{
  orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

  orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);

  orxViewport_CreateFromConfig("Viewport");

  ship = orxObject_CreateFromConfig("Ship");
  shipBulletSpawnerOwner = orxObject_GetOwnedChild(orxObject_GetOwnedChild(ship));
  orxObject_Enable(shipBulletSpawnerOwner, orxFALSE);

  alienSpawnerOwner = orxObject_CreateFromConfig("AlienSpawnerOwner");
  orxObject_CreateFromConfig("Scene");

  return orxSTATUS_SUCCESS;
}

int main(int argc, char **argv)
{
  orx_Execute(argc, argv, Init, Run, Exit);
  return EXIT_SUCCESS;
}

#ifdef __orxMSVC__

// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  // Inits and executes orx
  orx_WinExecute(Init, Run, Exit);

  // Done!
  return EXIT_SUCCESS;
}

#endif // __orxMSVC__
