#include "orx.h"

orxOBJECT *shipObject;
orxSPAWNER *shipBulletSpawner;
orxOBJECT *alienSpawnerOwner;

/** Run callback for standalone
 */
orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;
  
  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  return eResult;
}


orxSTATUS orxFASTCALL InputEventHandler(const orxEVENT *_pstEvent)
{
	if (shipObject == orxNULL)
		return orxSTATUS_SUCCESS;
	
	if (_pstEvent->eType == orxEVENT_TYPE_INPUT) {

		orxFLOAT rotation = 3;
		
		if (orxInput_IsActive("GoAnticlockwiseKey") ){
			orxObject_SetAngularVelocity(shipObject, -rotation);	
		}

		if (orxInput_IsActive("GoClockwiseKey") ){
			orxObject_SetAngularVelocity(shipObject, rotation);	
		}
		
		if (orxInput_IsActive("Fire") && orxInput_HasNewStatus("Fire") ){
			orxSpawner_Enable(shipBulletSpawner, orxTRUE);
		}
		
		if (orxInput_IsActive("Fire") == orxFALSE && orxInput_HasNewStatus("Fire") ){
			orxSpawner_Enable(shipBulletSpawner, orxFALSE);
		}
	}
}

void CreateExplosionAtObject(orxOBJECT *object)
{
	if (object == orxNULL)
		return;

	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;

	orxOBJECT *explosion = orxObject_CreateFromConfig("ExplosionSpawnerOwner");
	
	orxObject_SetPosition(explosion, &objectVector);
	
	/* Rotation copied so that it starts at exactly the same place as the alien */
	orxFLOAT rotation = orxObject_GetRotation(object);
	orxObject_SetRotation(explosion, rotation);
}

orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "ShipBulletObject") == 0
			    &&
			    orxString_Compare(orxObject_GetName(pstRecipientObject), "AlienObject") == 0
			   ) {
				CreateExplosionAtObject(pstRecipientObject);
				orxObject_SetLifeTime(pstSenderObject, 0);
				orxObject_SetLifeTime(pstRecipientObject, 0);
			}

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "ShipBulletObject") == 0
			    &&
			    orxString_Compare(orxObject_GetName(pstSenderObject), "AlienObject") == 0
			   ) {
				CreateExplosionAtObject(pstSenderObject);
				orxObject_SetLifeTime(pstSenderObject, 0);
				orxObject_SetLifeTime(pstRecipientObject, 0);
			}
					
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "AlienObject") == 0
			    &&
			    orxString_Compare(orxObject_GetName(pstRecipientObject), "WallObject") == 0
			   ) {
				orxEvent_RemoveHandler(orxEVENT_TYPE_INPUT, InputEventHandler);
				orxEvent_RemoveHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	
				CreateExplosionAtObject(shipObject);
				orxObject_SetLifeTime(shipObject, 0);
				orxObject_SetLifeTime(alienSpawnerOwner, 0);
			}

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "AlienObject") == 0
			    &&
			    orxString_Compare(orxObject_GetName(pstSenderObject), "WallObject") == 0
			   ) {
				orxEvent_RemoveHandler(orxEVENT_TYPE_INPUT, InputEventHandler);
				orxEvent_RemoveHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
				
				CreateExplosionAtObject(shipObject);
				orxObject_SetLifeTime(shipObject, 0);
				orxObject_SetLifeTime(alienSpawnerOwner, 0);
			}
			
			
			
			
			
		}
		

	}
}

void orxFASTCALL Exit()
{ 
	orxLOG("=================== EXIT ================");
}


/** Inits the tutorial
 */
orxSTATUS orxFASTCALL Init()
{
	
	orxVIEWPORT *pstViewport;
	pstViewport =	orxViewport_CreateFromConfig("Viewport");

	orxEvent_AddHandler(orxEVENT_TYPE_INPUT, InputEventHandler);
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
	
	/* Gets camera */
	orxViewport_GetCamera(pstViewport);
	
	shipObject = orxObject_CreateFromConfig("ShipObject");
	//orxOBJECT *shipshadow = orxOBJECT(orxObject_GetChild(shipObject));
	//shipBulletSpawner = orxSPAWNER(orxObject_GetSibling(shipshadow));
	//orxSpawner_Enable(shipBulletSpawner, orxFALSE);
	//alienSpawnerOwner = orxObject_CreateFromConfig("AlienSpawnerOwner");
	//orxObject_CreateFromConfig("WallObject");
	//orxObject_CreateFromConfig("StarSpawnerOwner");
	
	return orxSTATUS_SUCCESS; 
	
}




int main(int argc, char **argv)
{
	orx_Execute(argc, argv, Init, Run, Exit);
	return EXIT_SUCCESS;
}

#ifdef __orxMSVC__

// Here's an example for a console-less program under windows with visual studio
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  // Inits and executes orx
  orx_WinExecute(Init, Run, Exit);

  // Done!
  return EXIT_SUCCESS;
}

#endif // __orxMSVC__

/* Registers plugin entry */
//orxPLUGIN_DECLARE_ENTRY_POINT(Init);


