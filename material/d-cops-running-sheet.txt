1. 5:20 Powerpoint
	About Me.
	Where I work
	I like writing games for Balance and mental health, breaking out..
	Questions at the end if time.
	
	
2. Look at a project stucture - what are we building

3. Explain the default config and default source code

Start
-----


Define the graphics from the spritesheet.

[SpriteGraphic]
Texture         = sprites.png


[ShipGraphic@SpriteGraphic]
TextureCorner   = (0, 0, 0)
TextureSize     = (8, 8, 0)
Pivot           = center


[ShipBulletGraphic@SpriteGraphic]
TextureCorner   = (8, 0, 0)
TextureSize     = (8, 8, 0)
Pivot           = center


---


Move TextureSize     = (8, 8, 0) to [SpriteGraphic]


---


Make a ShipObject so something can be seen onscreen

[ShipObject]
Graphic         = ShipGraphic
Position        = (400, 300, 0)
Scale		= (4, 4, 0);

orxObject_CreateFromConfig("ShipObject");


---



Make:

[ObjectDefaults]

move the stuff to it.



---


The ship needs to be owned by another object and be offset so it can go around a circle.

[ShipRotator]
ChildList 	= ShipObject
Position	= (400, 300, 0)

[ShipObject@ObjectDefaults]
...
Position	= (0, 260, 0)
...

	orxObject_CreateFromConfig("ShipRotator");


---


The type of spin needs physics for a nice slow down effect. We need Keys first.

KEY_LEFT		= GoAntiClockwise
KEY_RIGHT		= GoClockwise


---


Now to respond to those keys

First, jack into orx's existing core clock, and send it to a function of our choosing:

orxClock_Register(orxClock_FindFirst(orx2F(-1.0f), orxCLOCK_TYPE_CORE), Update, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);




void orxFASTCALL Update(const orxCLOCK_INFO *_pstClockInfo, void *_pContext)
{
	orxLOG("Test");
}



---


Add keys and ship decleration:

  if (shipRotator) {

    orxFLOAT rotation = 3;

    if (orxInput_IsActive("GoAntiClockwise") ) {
      orxObject_SetAngularVelocity(shipRotator, -rotation);
    }

    if (orxInput_IsActive("GoClockwise") ) {
      orxObject_SetAngularVelocity(shipRotator, rotation);
    }

  }



Declare it first:

orxOBJECT *shipRotator;


shipRotator = orxObject_CreateFromConfig("ShipRotator");


---


Run and no slowdown because it has no physical body:

[ShipRotator]
...
...
Body        = ShipRotatorBody

[ShipRotatorBody]
PartList        = ShipRotatorBodyPart
AngularDamping    = 5
Dynamic         = true

[ShipRotatorBodyPart]
Type            = box
Solid           = false


---

Now time for shooting. We'll need a bullet object and a spawner attached to another child object:
I could do it without the extra, child but this makes it easier to enable/disable without bothering our ship.

[ShipBulletObject]
Graphic         = ShipBulletGraphic
Speed           = (0, -400, 0)
LifeTime        = 0.120

[ShipObject]
...
...
...
ChildList	= ShipBulletSpawnerOwner ;so that it can be enabled / disabled


[ShipBulletSpawnerOwner] ;object to be enabled / disabled
Spawner		= ShipBulletSpawner


[ShipBulletSpawner]
Object          = ShipBulletObject
WaveSize        = 1
WaveDelay       = 0.02
ActiveObject    = 1
Position        = (0, -4, 0)
UseSelfAsParent = true


---


The ship should't shoot by itself. I'll turn it off during init, and we'll set up some keys:


orxOBJECT *shipObject;
orxOBJECT *shipBulletSpawnerOwner;


...
	shipObject = orxObject_GetOwnedChild(shipRotator);
	shipBulletSpawnerOwner = orxObject_GetOwnedChild(shipObject);
	
	orxObject_Enable(shipBulletSpawnerOwner, orxFALSE);



Do the key;

KEY_LCTRL       = Fire

    if (orxInput_HasNewStatus("Fire")) {
      orxObject_Enable(shipBulletSpawnerOwner, orxInput_IsActive("Fire"));
    }



---

That was the trickiest bit. Now to move onto the aliens coming out:



[AlienGraphic@SpriteGraphic]
TextureCorner   = (0, 8, 0)
Pivot           = center top

[AlienObject@ObjectDefaults]
Graphic         = AlienGraphic
Speed           = (0,8,0) ~ (0,30,0)
LifeTime        = 12
Rotation        = 0 ~ 360
UseRelativeSpeed= true

[AlienSpawnerOwner]
Position        =   (400, 300, 0.1)
Spawner         = AlienSpawner

[AlienSpawner]
Object          = AlienObject
WaveSize        = 1
WaveDelay       = 0.4
Position        = (0, 0, 0)



	orxObject_CreateFromConfig("AlienSpawnerOwner");



---

Next is to make the bullets destroy the aliens. We need a physics handler:

	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);	


orxSTATUS orxFASTCALL PhysicsEventHandler(const orxEVENT *_pstEvent)
{
  if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

    if (_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
      orxOBJECT *pstRecipientObject, *pstSenderObject;

      /* Gets colliding objects */
      pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
      pstSenderObject = orxOBJECT(_pstEvent->hSender);

    }
  }

  return orxSTATUS_SUCCESS;
}

---

So far it does nothing, and anyway, our bullets and aliens need a physics body.


[ShipBulletObject]
...

...

[ShipBulletBody]
PartList        = ShipBulletBodyPart
Dynamic         = true 

[ShipBulletBodyPart]
Type            = box
Friction        = 0.0
SelfFlags       = bullet
CheckMask       = alien


[AlienObject@ObjectDefaults]
...
Body            = AlienBody
...


[AlienBody]
PartList        = AlienBodyPart
Dynamic         = true

[AlienBodyPart]
Type            = box
Friction        = 0.0
SelfFlags       = alien
CheckMask       = bullet # ship # wall
Solid           = false


      if (orxString_Compare(orxObject_GetName(pstSenderObject), "ShipBulletObject") == 0) {
        orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }
      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "ShipBulletObject") == 0) {
        orxObject_SetLifeTime(pstSenderObject, 0);
        orxObject_SetLifeTime(pstRecipientObject, 0);
      }


---


You can see the physics debugger, but can turn it off at this point.


---


Erasing the aliens isn't much fun, so its time to make the explosion. First, the graphic frames:


[ExplosionGraphic@SpriteGraphic]
Pivot           = center top

[ExplosionGraphic01@ExplosionGraphic]
TextureCorner   = (16, 0, 0)
[ExplosionGraphic02@ExplosionGraphic]
TextureCorner   = (24, 0, 0)
[ExplosionGraphic03@ExplosionGraphic]
TextureCorner   = (32, 0, 0)
[ExplosionGraphic04@ExplosionGraphic]
TextureCorner   = (48, 0, 0)
[ExplosionGraphic05@ExplosionGraphic]
TextureCorner   = (56, 0, 0)
[ExplosionGraphic06@ExplosionGraphic]
TextureCorner   = (64, 0, 0)
[ExplosionGraphic07@ExplosionGraphic]
TextureCorner   = (16, 8, 0)
[ExplosionGraphic08@ExplosionGraphic]
TextureCorner   = (24, 8, 0)
[ExplosionGraphic09@ExplosionGraphic]
TextureCorner   = (32, 8, 0)
[ExplosionGraphic10@ExplosionGraphic]
TextureCorner   = (48, 8, 0)
[ExplosionGraphic11@ExplosionGraphic]
TextureCorner   = (56, 8, 0)
[ExplosionGraphic12@ExplosionGraphic]
TextureCorner   = (64, 8, 0)

[ExplodeAnimationSet]
AnimationList   = ExplosionAnim

[ExplosionAnim]
DefaultKeyDuration = 0.02
KeyData1        = ExplosionGraphic01
KeyData2        = ExplosionGraphic02
KeyData3        = ExplosionGraphic03
KeyData4        = ExplosionGraphic04
KeyData5        = ExplosionGraphic05
KeyData6        = ExplosionGraphic06
KeyData7        = ExplosionGraphic07
KeyData8        = ExplosionGraphic08
KeyData9        = ExplosionGraphic09
KeyData10       = ExplosionGraphic10
KeyData11       = ExplosionGraphic11
KeyData12       = ExplosionGraphic12

[ExplosionParticle@ObjectDefaults]
AnimationSet    = ExplodeAnimationSet
Graphic         = ExplosionGraphic01
LifeTime        = 0.2
Speed           = (-150, -150, 0) ~ (150, 150, 0)
Alpha           = 0.5
BlendMode       = add
Scale           = 1.0 ~ 4.0

[Explosion]
Spawner			= ExplosionSpawner

[ExplosionSpawner]
Object          = ExplosionParticle
WaveSize        = 1
WaveDelay       = 0.01
ActiveObject    = 8
TotalObject     = 8


void CreateExplosionAtObject(orxOBJECT *object)
{
  if (object == orxNULL)
    return;

  orxVECTOR objectVector;
  orxObject_GetWorldPosition(object, &objectVector);
  objectVector.fZ = -0.1;

  orxOBJECT *explosion = orxObject_CreateFromConfig("Explosion");

  orxObject_SetPosition(explosion, &objectVector);

  /* Rotation copied so that it starts at exactly the same place as the alien */
  orxFLOAT rotation = orxObject_GetRotation(object);
  orxObject_SetRotation(explosion, rotation);
}



      if (orxString_Compare(orxObject_GetName(pstSenderObject), "ShipBulletObject") == 0) {
		CreateExplosionAtObject(pstRecipientObject);


      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "ShipBulletObject") == 0) {
		CreateExplosionAtObject(pstSenderObject);


---


Now still a little boring. Time to get sounds in.




[SoundDefaults]
KeepInCache     = true
Volume          = 0.9
Attenuation     = 0.2

[AlienAppearSound@SoundDefaults]
Sound           = alienappear.ogg

[ShootSound@SoundDefaults]
Sound           = shoot.ogg

[ExplodeSound@SoundDefaults]
Sound           = explode.ogg



[ShipBulletObject]
...
SoundList       = ShootSound

[AlienObject@ObjectDefaults]
...
SoundList       = AlienAppearSound

[ExplosionParticle@ObjectDefaults]
...
SoundList       = ExplodeSound


---


What if the aliens get to the edge of the screen? We'll make it that if they reach outer invisible walls, then the game is over:



;outside wall
[WallObject]
Position        = (-80,-50,0)
Body            = WallBody
Scale           = 1.2

[WallBody]
PartList        = WallBodyPartA # WallBodyPartB # WallBodyPartC # WallBodyPartD

[WallBodyPartA]
Type            = mesh
SelfFlags       = wall
CheckMask       = alien
Solid           = false
VertexList      = (0,0,0) # (400,0,0) # (0,300,0)

[WallBodyPartB@WallBodyPartA]
VertexList      = (400,0,0) # (800,0,0) # (800,300,0)

[WallBodyPartC@WallBodyPartA]
VertexList      = (0,300,0) # (400,600,0) # (0,600,0)

[WallBodyPartD@WallBodyPartA]
VertexList      = (800,300,0) # (800,600,0) # (400,600,0)



---


Then:

[Scene]
ChildList 	= WallObject


---


Because we don't need to do work with the walls in code, we can just load the walls in the init function:

orxObject_CreateFromConfig("Scene");


But they're invisible, turn on debug mode.


---

Now to set the collision between a wall and an alien:


      else if (orxString_Compare(orxObject_GetName(pstRecipientObject), "WallObject") == 0
           ||  orxString_Compare(orxObject_GetName(pstSenderObject), "WallObject") == 0) {
        orxEvent_RemoveHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandler);
        CreateExplosionAtObject(shipObject);
        orxObject_SetLifeTime(shipObject, 0);
        shipObject = orxNULL;
        orxObject_SetLifeTime(alienSpawnerOwner, 0);
        alienSpawnerOwner = orxNULL;
      }


And set the alien Spawner as a variable:



---

The game core is now complete. So for our remaining time, I'll add some simple candy. We need to have a starfield:

[StarSpawnerOwner]
Position        = (400, 300, 0.1)
Spawner         = StarSpawner

[StarSpawner]
Object          = StarObject
WaveSize        = 4
WaveDelay       = 0.2
Position        = (0, 0, 0)

[StarObject@ObjectDefaults]
Graphic         = ExplosionGraphic01
Scale           = 0.25 ~ 1
Speed           = (-150, -150, 0.1) ~ (150, 150, 0.1)
LifeTime        = 12
Rotation        = 0 ~ 360
UseRelativeSpeed= true


---


Now, to attach the stars to the scene object:


[Scene]
ChildList = ... # WallObject

---

For a final piece of candy, some drop shadows on the ship and alien:




[ShadowDefaults]
Color           = (0,0,0)
Position        = (1, 1, 0.1)
Alpha           = 0.25

[AlienShadowObject@ShadowDefaults]
Graphic         = AlienGraphic

[ShipShadowObject@ShadowDefaults]
Graphic         = ShipGraphic




[AlienObject@ObjectDefaults]
...
ChildList       = AlienShadowObject


[ShipObject@ObjectDefaults]
...
ChildList	= ...# ShipShadowObject

---