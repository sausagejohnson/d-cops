#include "orx.h"


/** Run callback every clock cycle 
 */
orxSTATUS orxFASTCALL Run()
{
  orxSTATUS eResult = orxSTATUS_SUCCESS;

  if(orxInput_IsActive("Quit")){
    eResult = orxSTATUS_FAILURE;
  }

  return eResult;
}


void orxFASTCALL Exit()
{
  orxLOG("=================== EXIT ================");
}


/** Inits the game
 */
orxSTATUS orxFASTCALL Init()
{
	orxViewport_CreateFromConfig("Viewport");
	
	return orxSTATUS_SUCCESS;
}


int main(int argc, char **argv)
{
  orx_Execute(argc, argv, Init, Run, Exit);
  return EXIT_SUCCESS;
}

